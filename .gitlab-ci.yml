include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

stages:
  - prerequisite
  - build
  - deploy
  - cleanup

variables:
  STAGING_IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_IID}-"
  IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/images:"
  GIT_DEPTH: 1
  REG_SHA256: "ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228"
  REG_VERSION: "0.16.1"

vanilla targets:
  stage: prerequisite
  image: alpine
  before_script:
    - apk update
    - apk add file
  script:
    # <a href="v1.3/">v1.3/</a> 20-Mar-2003 23:02 -
    - wget -q -O dump https://cdn.kernel.org/pub/linux/kernel
    - major_dirs=$({ if file dump | grep -q gzip; then
                       gzip -cd dump;
                     else
                       cat dump;
                     fi;
                   } | awk '/href="v\d\..\/"/ { print $0}' | cut -d\" -f2)
    # <a href="linux-5.9.5.tar.xz">linux-5.9.5.tar.xz</a> 05-Nov-2020 10:57 110M
    - for md in ${major_dirs}; do
        wget -q -O dump https://cdn.kernel.org/pub/linux/kernel/${md};
        { if file dump | grep -q gzip; then
            gzip -cd dump;
          else
            cat dump;
          fi;
        } |
          (grep -s -E 'linux-\d+\.\d+(\.\d+)*\.tar\.xz' || true) |
          cut -d\" -f2 |
          sed -e "s!linux-\(.*\).tar.xz!${md},\1!";
      done | sort -t , -k 2 -u -V > all
    - |
      { \
        for version in $(cat all | cut -d, -f2 | cut -d. -f1,2 | sort -u); do \
          if printf "4.16\n${version}" | sort -c -V 2>/dev/null; then \
            series="focal"; \
          elif printf "4.2\n${version}" | sort -c -V 2>/dev/null; then \
            case "${version}" in \
              "4.4"|"4.9"|"4.14") series="focal";; \
              *) series="xenial";; \
            esac; \
          elif printf "3.9\n${version}" | sort -c -V 2>/dev/null; then \
            case "${version}" in \
              "3.12"|"3.18") series="xenial";; \
              *) series="trusty";; \
            esac; \
          elif printf "2.6\n${version}" | sort -c -V 2>/dev/null; then \
            case "${version}" in \
              "3.2"|"3.4") series="trusty";; \
              *) series="precise";; \
            esac; \
          else \
            continue;
          fi; \
          latest=$(grep -E ",${version%.*}\.${version#*.}(\.|$)" all | tail -n1); \
          printf "vanilla:${series}:${latest#*,}:x86_64:\n"; \
          printf "  extends: .build-image\n"; \
          printf "  variables:\n"; \
          printf "    KERNEL_ARCHIVE: \"https://cdn.kernel.org/pub/linux/kernel/${latest%,*}linux-${latest#*,}.tar.xz\"\n\n"; \
        done; \
      } >vanilla-targets.yml
  artifacts:
    paths:
      - vanilla-targets.yml

vanilla images:
  stage: build
  needs:
    - vanilla targets
  variables:
    TEST_SERIAL: "${CI_PIPELINE_IID}"
  trigger:
    include:
      - local: .pipeline-vanilla.yml
      - artifact: vanilla-targets.yml
        job: vanilla targets
    strategy: depend

.deploy-template: &deploy-template
  stage: deploy
  image: docker:git
  services:
    - docker:dind
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add --no-cache curl
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v${REG_VERSION}/reg-linux-amd64" --output /usr/local/bin/reg
    - echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg

    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
  script:
    - |
      staging_tags=$(/usr/local/bin/reg tags --auth-url "${CI_REGISTRY}" \
          -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" \
          "${STAGING_IMAGE_PREFIX%:*}" | grep "^${STAGING_IMAGE_PREFIX##*:}")
      for staging_tag in ${staging_tags}; do
        staging_image="${STAGING_IMAGE_PREFIX%:*}:${staging_tag}"
        flavor="${staging_tag#*-}";
        kver="${flavor#*-}";
        kver="${kver%-*}";
        arch="${flavor##*-}";
        flavor="${flavor%%-*}";
        tags="${flavor}-${kver}-${arch}";
        test "${arch}" != "x86_64" || tags="${tags} ${flavor}-${kver}";
        if [ "${flavor}" = "vanilla" ]; then
          tags="${tags} ${kver}-${arch}";
          test "${arch}" != "x86_64" || tags="${tags} ${kver}";
        fi;

        echo "### To push ${staging_image} as ${tags} ###";
        test "${CI_JOB_STAGE}" = "deploy" || continue;

        docker pull --quiet "${staging_image}";
        for tag in ${tags}; do \
          image="${IMAGE_PREFIX}${tag}";
          docker tag "${staging_image}" "${image}";
          docker push --quiet "${image}";
          docker rmi "${image}";
        done
        docker rmi "${staging_image}";
      done

test deploy:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

deploy:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

cleanup:
  stage: cleanup
  image: docker
  services:
    - docker:dind
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add --no-cache curl
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v${REG_VERSION}/reg-linux-amd64" --output /usr/local/bin/reg
    - echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
  script:
    - |
      staging_tags=$(/usr/local/bin/reg tags --auth-url "${CI_REGISTRY}" \
          -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" \
          "${STAGING_IMAGE_PREFIX%:*}" | grep "^${STAGING_IMAGE_PREFIX##*:}")
      for tag in ${staging_tags}; do
        /usr/local/bin/reg rm -d --auth-url "${CI_REGISTRY}" \
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" \
            "${STAGING_IMAGE_PREFIX%:*}:${tag}" || true;
      done
  rules:
    - when: always
