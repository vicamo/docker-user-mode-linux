ARG IMAGE_SERIES=focal

FROM registry.gitlab.com/echoii/ci/kteam-tools/chroots:$IMAGE_SERIES-amd64 AS builder

ARG KERNEL_ARCH=x86_64
ARG KERNEL_VERSION
ARG KERNEL_ARCHIVE

ENV KERNEL_ARCH=$KERNEL_ARCH \
	KERNEL_VERSION=$KERNEL_VERSION

RUN apt-get update --quiet \
	&& apt-get install --no-install-recommends --yes \
		ca-certificates \
		wget \
	&& (wget -O - --no-verbose "${KERNEL_ARCHIVE}" | tar -C / -Jxf -) \
	&& cd "/linux-${KERNEL_VERSION}" \
	&& make mrproper \
	&& make defconfig ARCH=um SUBARCH="${KERNEL_ARCH}" \
	&& make linux ARCH=um SUBARCH="${KERNEL_ARCH}" -j$(nproc) \
	&& make modules ARCH=um SUBARCH="${KERNEL_ARCH}" \
	&& make modules_install ARCH=um SUBARCH="${KERNEL_ARCH}" \
	&& mv linux /

FROM ubuntu:$IMAGE_SERIES

ARG KERNEL_ARCH=x86_64
ARG KERNEL_VERSION
ARG LABEL_IMAGE_FLAVOR
ARG LABEL_IMAGE_CREATED
ARG LABEL_IMAGE_VERSION
ARG LABEL_IMAGE_REVISION

LABEL org.opencontainers.image.title="user-mode-linux" \
	org.opencontainers.image.description="Docker image with user-mode-linux binary" \
	org.opencontainers.image.authors="You-Sheng Yang" \
	org.opencontainers.image.url="https://gitlab.com/vicamo/docker-user-mode-linux" \
	org.opencontainers.image.documentation="https://gitlab.com/vicamo/docker-user-mode-linux/-/blob/master/README.md" \
	org.opencontainers.image.created="$LABEL_IMAGE_CREATED" \
	org.opencontainers.image.source="https://gitlab.com/vicamo/docker-user-mode-linux.git" \
	org.opencontainers.image.version="$LABEL_IMAGE_VERSION" \
	org.opencontainers.image.revision="$LABEL_IMAGE_REVISION" \
	org.opencontainers.image.ref.name="registry.gitlab.com/vicamo/docker-user-mode-linux/images:$LABEL_IMAGE_FLAVOR-$KERNEL_VERSION-$KERNEL_ARCH" \
	org.opencontainers.image.licenses="Apache-2.0"

ENV KERNEL_ARCH=$KERNEL_ARCH \
	KERNEL_VERSION=$KERNEL_VERSION

COPY --from=builder /linux /usr/bin/linux.uml
RUN update-alternatives --install /usr/bin/linux linux /usr/bin/linux.uml 10

COPY --from=builder /lib/modules /lib/modules

RUN apt-get update --quiet \
	&& apt-get install --no-install-recommends --yes \
		uml-utilities \
	&& rm -rf /var/lib/apt/lists/*_dists_*
